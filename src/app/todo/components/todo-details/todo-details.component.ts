import {Component, HostBinding, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {TodoService} from '@app/todo/services/todo.service';
import {ITodo} from '@app/core/interfaces/todo.interface';
import {fader} from '@app/core/animation';

@Component({
    selector: 'app-todo-details',
    templateUrl: './todo-details.component.html',
    styleUrls: ['./todo-details.component.scss'],
    animations: [
        fader
    ],

})
export class TodoDetailsComponent implements OnInit {

    constructor(private router: Router, private route: ActivatedRoute, private todoService: TodoService) {
    }
    @HostBinding('@routeAnimations') routeAnimations = true;

    destroySubject$: Subject<void> = new Subject<void>();
    todo$: Observable<ITodo>;

    ngOnInit() {
        this.route.params
            .pipe(takeUntil(this.destroySubject$))
            .subscribe(params => {
                this.todo$ = this.todoService.getTodoById(+params.id);
            });
    }

    backToList() {
        this.router.navigate(['/todo']);
    }

}
