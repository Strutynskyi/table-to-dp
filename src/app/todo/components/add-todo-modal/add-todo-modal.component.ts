import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ITodo} from '@app/core/interfaces/todo.interface';
import {TodoListComponent} from '@app/todo/components/todo-list/todo-list.component';

@Component({
    selector: 'app-add-todo-modal',
    templateUrl: './add-todo-modal.component.html',
    styleUrls: ['./add-todo-modal.component.scss']
})
export class AddTodoModalComponent implements OnInit {
    addTodoForm: FormGroup;

    constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<TodoListComponent>,
                @Inject(MAT_DIALOG_DATA) public data: ITodo | null) {
    }

    ngOnInit() {
        this.initAddTodoForm();

        if (this.data) {
            this.addTodoForm = this.fb.group({
                name: [this.data.name, [Validators.required]],
                description: [this.data.description],
                isCompleted: [this.data.isCompleted],
                date: ['2019-11-21T22:00:00.000Z']
            });
        }
    }

    initAddTodoForm() {
        this.addTodoForm = this.fb.group({
            name: [null, [Validators.required]],
            description: [null],
            isCompleted: [false],
            date: [null, [Validators.required]],
        });
    }

    onSubmit() {
        this.dialogRef.close(this.addTodoForm.value);
    }

    closeWindow() {
        this.dialogRef.close();
    }
}
