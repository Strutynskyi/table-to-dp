import {Component, HostBinding, OnInit, Output} from '@angular/core';
import { TodoService } from '@app/todo/services/todo.service';
import {ITodo} from '@app/core/interfaces/todo.interface';
import {Observable} from 'rxjs';
import {AuthService} from '@auth/services/auth.service';
import {fader} from '@app/core/animation';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  animations: [fader],

})
export class TodoComponent implements OnInit {
  @HostBinding('@routeAnimations') routeAnimations = true;
  todoList$: Observable<ITodo[]>;

  constructor(private todoService: TodoService, private authService: AuthService) { }

  ngOnInit() {
      this.todoList$ = this.todoService.getTodos();
  }

  logout() {
    this.authService.logout();
  }
}
