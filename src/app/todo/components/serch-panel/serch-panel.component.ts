import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {fromEvent} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {MatCheckboxChange} from '@angular/material';

@Component({
    selector: 'app-search-panel',
    templateUrl: './serch-panel.component.html',
    styleUrls: ['./serch-panel.component.scss'],
})
export class SearchPanelComponent implements OnInit, AfterViewInit {
    @ViewChild('searchInput', {static: false}) searchInput: ElementRef;
    @Output() searchEvent = new EventEmitter<string>()
    @Output() checkBoxEvent = new EventEmitter<boolean>()
    @Output() sortByDateEvent = new EventEmitter<boolean>()
    isCheckBoxChecked: boolean = false;
    isSortByDateChecked: boolean = false;
    keyWord: string = null;

    constructor() {
    }

    conductSearch(keyWord: string) {
        this.isCheckBoxChecked = false;
        this.isSortByDateChecked = false;
        this.searchEvent.emit(keyWord);
    }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(debounceTime(700),
                map((event: any) => event.target.value),
                distinctUntilChanged())
            .subscribe(res => {
                this.conductSearch(res);
            });
    }

    getCompletedOnly(event: MatCheckboxChange): void {
        this.keyWord = null;
        this.isCheckBoxChecked = event.checked;
        this.checkBoxEvent.emit(event.checked);
    }

    sortByDate(event: MatCheckboxChange): void {
        this.isSortByDateChecked = event.checked;
        this.sortByDateEvent.emit(event.checked);
    }
}
