import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoComponent } from './components/todo/todo.component';
import { TodoRoutingModule } from '@app/todo/todo-routing.module';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { MaterialModule } from '@app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddTodoModalComponent } from './components/add-todo-modal/add-todo-modal.component';
import {SearchPanelComponent} from '@app/todo/components/serch-panel/serch-panel.component';
import { TodoDetailsComponent } from './components/todo-details/todo-details.component';



@NgModule({
  declarations: [
      TodoComponent,
      TodoListComponent,
      TodoItemComponent,
      AddTodoModalComponent,
      SearchPanelComponent,
      TodoDetailsComponent
  ],
  imports: [
    CommonModule,
    TodoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
    entryComponents: [
        AddTodoModalComponent
    ]
})
export class TodoModule { }
