import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TodoComponent} from '@app/todo/components/todo/todo.component';
import {TodoDetailsComponent} from '@app/todo/components/todo-details/todo-details.component';


const routes: Routes = [
    {
        path: '',
        component: TodoComponent,
    },
    {
        path: ':id',
        component: TodoDetailsComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class TodoRoutingModule {
}
