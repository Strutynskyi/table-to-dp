import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {ITodo} from '@app/core/interfaces/todo.interface';

@Injectable({
    providedIn: 'root'
})
export class TodoService {
    readonly endpoint = environment.api;

    constructor(private http: HttpClient) {
    }

    getTodos(): Observable<ITodo[]> {
        return this.http.get<ITodo[]>(`${this.endpoint}/todos`);
    }

    addTodo(todo: ITodo): Observable<ITodo> {
        return this.http.post<ITodo>(`${this.endpoint}/todos`, todo);
    }

    editTodo(id: number, todo: ITodo): Observable<ITodo> {
        return this.http.put<ITodo>(`${this.endpoint}/todos/${id}`, todo);
    }

    deleteTodo(id: number): Observable<ITodo> {
        return this.http.delete<ITodo>(`${this.endpoint}/todos/${id}`);
    }

    getTodoById(id: number): Observable<ITodo> {
        return this.http.get<ITodo>(`${this.endpoint}/todos/${id}`);
    }
}
