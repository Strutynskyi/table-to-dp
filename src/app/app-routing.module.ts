import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions, PreloadAllModules } from '@angular/router';
import { AuthGuard } from '@auth/guards/auth.guard';
import {fader} from '@app/core/animation';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'todo',
    },
    {
        path: 'todo',
        loadChildren: './todo/todo.module#TodoModule',
        canActivate: [AuthGuard],
        data: { animation: 'fader' }
    },
    {
        path: 'login',
        loadChildren: './auth/auth.module#AuthModule',
    },

    {path: '**', redirectTo: '', pathMatch: 'full'}
];

const extraOptions: ExtraOptions = {
    preloadingStrategy: PreloadAllModules
};

@NgModule({
    imports: [RouterModule.forRoot(routes, extraOptions)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
