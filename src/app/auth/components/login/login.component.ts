import {Component, HostBinding, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '@app/auth/services/auth.service';
import { Router } from '@angular/router';
import { IUser } from '@auth/models/user.model';
import {fader} from '@app/core/animation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
    animations: [
        fader
    ]
})

export class LoginComponent implements OnInit {
  @HostBinding('@routeAnimations') routeAnimations = true;
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
      if (authService.isAuthenticated()) {
          router.navigate(['/todo']);
      }
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      login: [null, [Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.email ]],
      password: [null, [Validators.required, Validators.minLength(8), Validators.pattern(/(?=.*\d)(?=.*[A-Z])/)]]
    });
}

  onSubmit() {
    const user: IUser = this.loginForm.value;
    this.authService.login(user);
  }
}
