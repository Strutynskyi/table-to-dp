import {animate, group, keyframes, query, style, transition, trigger} from '@angular/animations';

export const fader =
    trigger('routeAnimations', [
        transition('* <=> *', [
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    left: 0,
                    width: '100%',
                }),
            ], { optional: true }),
            group([
                query(':enter', [
                    animate('2000ms ease', keyframes([
                        style({ transform: 'scale(0)', offset: 0 }),
                        style({ transform: 'scale(0.5)', offset: 0.3 }),
                        style({ transform: 'scale(1) translateX(0%)', offset: 1 }),
                    ])),
                ], { optional: true }),
                query(':leave', [
                    animate('2000ms ease', keyframes([
                        style({ transform: 'scale(1)', offset: 0 }),
                        style({ transform: 'scale(0.5)', offset: 0.35 }),
                        style({ opacity: 0, transform: 'scale(0)', offset: 1 }),
                    ] )),
                ], { optional: true })
            ]),
        ])

    ]);
