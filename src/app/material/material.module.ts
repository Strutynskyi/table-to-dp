import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSidenavModule,
    MatSelectModule,
    MatListModule,
    MatToolbarModule,
    MatRadioModule,
    MatSortModule,
    MatPaginatorModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatExpansionModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule
} from '@angular/material';
import {MatSnackBarModule} from '@angular/material/snack-bar';


@NgModule({
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatIconModule,
      MatListModule,
      MatSidenavModule,
      MatToolbarModule,
      MatSelectModule,
      MatRadioModule,
      MatCheckboxModule,
      MatDialogModule,
      MatTableModule,
      MatPaginatorModule,
      MatSortModule,
      MatExpansionModule,
      MatTabsModule,
      MatSnackBarModule,
      MatNativeDateModule
  ],

  exports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
      MatListModule,
      MatSidenavModule,
      MatToolbarModule,
      MatSelectModule,
      MatRadioModule,
      MatCheckboxModule,
      MatDialogModule,
      MatTableModule,
      MatPaginatorModule,
      MatSortModule,
      MatExpansionModule,
      MatTabsModule,
      MatSnackBarModule,
      MatDatepickerModule,
      MatNativeDateModule
  ]
})
export class MaterialModule { }
