import {Component, HostBinding, HostListener, Output} from '@angular/core';
import {fader} from '@app/core/animation';
import {RouterOutlet} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fader],
})

export class AppComponent {
    @Output() routeAnimations: boolean;

    prepareRoute(outlet: RouterOutlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    }

}
